package com.softwaymedical.test.converters;

import com.softwaymedical.converters.RomanConverter;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class RomanConverterTest {

    @Test
    public void numberConvertToOneCharacter() {
        assertThat(RomanConverter.convert(1), is("I"));
        assertThat(RomanConverter.convert(5), is("V"));
        assertThat(RomanConverter.convert(10), is("X"));
        assertThat(RomanConverter.convert(50), is("L"));
        assertThat(RomanConverter.convert(100), is("C"));
        assertThat(RomanConverter.convert(500), is("D"));
        assertThat(RomanConverter.convert(1000), is("M"));
    }

    @Test
    public void numberConvertToTwoSameCharacters() {
        assertThat(RomanConverter.convert(2), is("II"));
        assertThat(RomanConverter.convert(20), is("XX"));
        assertThat(RomanConverter.convert(200), is("CC"));
        assertThat(RomanConverter.convert(2000), is("MM"));
    }

    @Test
    public void numberConvertToThreeSameCharacters() {
        assertThat(RomanConverter.convert(3), is("III"));
        assertThat(RomanConverter.convert(30), is("XXX"));
        assertThat(RomanConverter.convert(300), is("CCC"));
        assertThat(RomanConverter.convert(3000), is("MMM"));
    }

    @Test
    public void numberConvertToTwoDifferentCharacters() {
        assertThat(RomanConverter.convert(11), is("XI"));
        assertThat(RomanConverter.convert(51), is("LI"));
        assertThat(RomanConverter.convert(9), is("IX"));
        assertThat(RomanConverter.convert(27), is("XXVII"));
        assertThat(RomanConverter.convert(434), is("CDXXXIV"));
        assertThat(RomanConverter.convert(1995), is("MCMXCV"));
    }
}
