package com.softwaymedical.converters;

import java.util.LinkedHashMap;
import java.util.List;

public class RomanConverter {

    private static LinkedHashMap<Integer, String> numberAssociations = new LinkedHashMap<>();
    private static List<String> a = List.of("I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX");
    private static List<String> b = List.of("X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC");
    private static List<String> c = List.of("C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM");
    private static List<String> d = List.of("M", "MM", "MMM");
    private static List<List<String>> abcd = List.of(a, b, c, d);

    static {
        numberAssociations.put(1000, "M");
        numberAssociations.put(500, "D");
        numberAssociations.put(100, "C");
        numberAssociations.put(50, "L");
        numberAssociations.put(10, "X");
        numberAssociations.put(5, "V");
        numberAssociations.put(1, "I");
    }
    public static String convertOld(int i) {
        StringBuilder result = new StringBuilder();
        while(i > 0) {
            for(Integer n: numberAssociations.keySet()) {
                if((i / n) > 0) {
                    result.append(numberAssociations.get(n).repeat(i / n));
                    i -= n * (i / n);
                }
            }
        }
        return result.toString();
    }

    public static String convert(int i) {
        StringBuilder result = new StringBuilder();
        String s = String.valueOf(i);
        for(int j = 0; j < s.length(); j++) {
            int numericValue = Character.getNumericValue(s.charAt(j));
            if(numericValue == 0) continue;
            result.append(abcd.get(s.length() - (j + 1)).get(numericValue - 1));
        }
        return result.toString();
    }
}

